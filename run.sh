#!/bin/bash

javac -d bin/ -cp src src/FizzBuzz.java
wget "http://search.maven.org/remotecontent?filepath=junit/junit/4.8.1/junit-4.8.1.jar" -O junit-4.8.1.jar
javac -cp junit-4.8.1.jar:src -d bin/ src/FizzBuzzTest.java
java -cp ./bin:junit-4.8.1.jar org.junit.runner.JUnitCore FizzBuzzTest
rm junit-4.8.1.jar
